<?php include_once 'includes/templates/header.php';

use PayPal\Api\ApiContext;
use PayPal\Api\PaymentExecution;
use PayPal\Api\Payment;

require 'includes/paypal.php';

?>
<section class="seccion contenedor">
    <h2>Resumen registro</h2>
    <?php
        $paymentId = $_GET['paymentId'];
        $idPago = (int) $_GET['id_pago'];

        $pago = Payment::get($paymentId, $apiContext);
        $execution = new PaymentExecution();
        $execution->setPayerId($_GET['PayerID']);
        $resultado = $pago->execute($execution, $apiContext);
        $respuesta = $resultado->transactions[0]->related_resources[0]->sale->state;

        if ($respuesta === 'completed') {
          echo "El pago se realizó correctamente. ";
          echo "El id es {$paymentId} ";
          require_once('includes/functions/bbdd_conexion.php');
          $stmt = $connection->prepare('UPDATE registrados SET pagado = ? WHERE ID_registrado = ?');
          $pagado = 1;
          $stmt->bind_param("ii", $pagado, $idPago);
          $stmt->execute();
          $ID_registro = $stmt->insert_id;
          $stmt->close();
          $connection->close();
        } else {
          echo "El pago no se realizó";
        }
    ?>
</section>

<?php include_once 'includes/templates/footer.php'; ?>