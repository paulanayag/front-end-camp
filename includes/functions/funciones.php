<?php
    function productos_json(&$boletos, &$camisetas = 0, &$etiquetas = 0) {
        $dias = array(0 => 'pase_dia', 1 => 'pase_completo', 2 => 'pase_dos_dias');
        $total_boletos = array_combine($dias, $boletos);
        $json = array();

        foreach($total_boletos as $key => $boleto):
            if((int) $boleto > 0):
                $json[$key] = (int) $boleto;
            endif;
        endforeach;

        $camisetas = (int) $camisetas;
        if($camisetas > 0):
            $json['camisetas'] = $camisetas;
        endif;

        $etiquetas = (int) $etiquetas;
        if($etiquetas > 0):
            $json['etiquetas'] = $etiquetas;
        endif;

        return json_encode($json);
    }
    function evento_json($eventos) {
        $eventos_json = array();
        if($eventos) {
            foreach($eventos as $evento):
                $eventos_json['eventos'][] = $evento;
            endforeach;
        }

        return json_encode($eventos_json);
    }
?>