<!doctype html>
<html class="no-js" lang="">

<head>
    <meta charset="utf-8">
    <title></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="manifest" href="site.webmanifest">
    <link rel="apple-touch-icon" href="icon.png">
    <!-- Place favicon.ico in the root directory -->

    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/all.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans|Oswald|PT+Sans&display=swap">
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.5.1/dist/leaflet.css" />
    <?php
        $archivo = basename($_SERVER['PHP_SELF']);
        $pagina = str_replace(".php", "", $archivo);
        if ($pagina === 'invitados' || $pagina === 'index') {
            echo '<link rel="stylesheet" href="css/colorbox.css" />';
        } else if ($pagina === 'conferencia') {
            echo '<link rel="stylesheet" href="css/lightbox.css" />';
        }
    ?>
    <link rel="stylesheet" href="css/main.css">
    <meta name="theme-color" content="#fafafa">
    <script id="mcjs">!function(c,h,i,m,p){m=c.createElement(h),p=c.getElementsByTagName(h)[0],m.async=1,m.src=i,p.parentNode.insertBefore(m,p)}(document,"script","https://chimpstatic.com/mcjs-connected/js/users/4ba748fdbc9f3a9af7f361af8/30d041a43fd15967f416798d5.js");</script>
</head>

<body class="<?php echo $pagina; ?>">
    <!--[if IE]>
    <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
<![endif]-->

    <!-- HEADER -->
    <header class="site-header">
        <div class="hero">
            <div class="contenido-header">
                <!-- REDES SOCIALES -->
                <nav class="redes-sociales">
                    <a href="#"><i class="fab fa-facebook-f"></i></a>
                    <a href="#"><i class="fab fa-twitter"></i></a>
                    <a href="#"><i class="fab fa-pinterest-p"></i></a>
                    <a href="#"><i class="fab fa-youtube"></i></a>
                    <a href="#"><i class="fab fa-instagram"></i></a>
                </nav>
                <!-- INFORMACIÓN EVENTO -->
                <div class="informacion-evento">
                    <div class="clearfix">
                        <p class="fecha"><i class="fas fa-calendar-alt"></i>11 Julio - 19 Agosto</p>
                        <p class="ciudad"><i class="fas fa-map-marker-alt"></i>Madrid</p>
                    </div>
                    <h1 class="nombre-web">FrontEndCamp</h1>
                    <p class="slogan">La mejor conferencia de <span>Diseño Web</span></p>
                </div>
            </div>
        </div>
    </header>

    <!-- BARRA NAVEGACIÓN -->
    <div class="barra">
        <div class="contenedor clearfix">
            <!-- LOGO -->
            <div class="logo">
                <a href="index.php">
                    <img src="img/logo.svg" alt="Logo">
                </a>
            </div>
            <!-- MENÚ -->
            <div class="menu-movil clearfix">
                <span></span>
                <span></span>
                <span></span>
            </div>
            <!-- NAVEGACIÓN PRINCIPAL -->
            <nav class="navegacion-principal">
                <a href="conferencia.php">Conferencia</a>
                <a href="calendario.php">Calendario</a>
                <a href="invitados.php">Invitados</a>
                <a href="registro.php">Registro</a>
            </nav>
        </div>
    </div>