    <!-- FOOTER -->
    <footer class="site-footer">
        <div class="contenedor clearfix">
            <div class="footer-informacion">
                <h3>Sobre <span>FrontEndCamp</span></h3>
                <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Deleniti excepturi exercitationem beatae quis dolore placeat quibusdam obcaecati a nisi repudiandae quia laboriosam assumenda deserunt illo, nihil quod esse culpa iste.</p>
            </div>
            <div class="ultimos-tweets">
                <h3>Últimos <span>tweets</span></h3>
                <ul>
                    <li>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Ipsam, sed optio? Deleniti harum beatae quos delectus maiores, consectetur, tempore rerum recusandae at esse placeat iste autem aliquam molestias aspernatur adipisci.</li>
                    <li>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Ipsam, sed optio? Deleniti harum beatae quos delectus maiores, consectetur, tempore rerum recusandae at esse placeat iste autem aliquam molestias aspernatur adipisci.</li>
                    <li>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Ipsam, sed optio? Deleniti harum beatae quos delectus maiores, consectetur, tempore rerum recusandae at esse placeat iste autem aliquam molestias aspernatur adipisci.</li>
                </ul>
            </div>
            <div class="menu">
                <h3>Redes <span>sociales</span></h3>
                <nav class="redes-sociales">
                    <a href="#"><i class="fab fa-facebook-f"></i></a>
                    <a href="#"><i class="fab fa-twitter"></i></a>
                    <a href="#"><i class="fab fa-pinterest-p"></i></a>
                    <a href="#"><i class="fab fa-youtube"></i></a>
                    <a href="#"><i class="fab fa-instagram"></i></a>
                </nav>
            </div>
        </div>
        <p class="copyright">
            Todos los derechos reservados FRONTENDCAMP 2020.
        </p>
        <!-- Begin Mailchimp Signup Form -->
        <link rel="stylesheet" href="css/mailchimp.css" type="text/css">
        <style type="text/css">
            #mc_embed_signup {background: #fff; clear: left; font: 14px Helvetica, Arial, sans-serif;}
        </style>
        <div style="display:none;">
            <div id="mc_embed_signup">
                <form action="https://gmail.us7.list-manage.com/subscribe/post?u=4ba748fdbc9f3a9af7f361af8&amp;id=79025bb69d" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                    <div id="mc_embed_signup_scroll">
                        <h2>Suscríbete a nuestra newsletter y no te pierdas nada de este evento</h2>
                        <div class="indicates-required"><span class="asterisk">* </span>campos obligatorios</div>
                        <div class="mc-field-group">
                            <label for="mce-EMAIL">Email<span class="asterisk">*</span>
                            </label>
                            <input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL">
                        </div>
                        <div class="mc-field-group">
                            <label for="mce-FNAME">Nombre</label>
                            <input type="text" value="" name="FNAME" class="" id="mce-FNAME">
                        </div>
                        <div class="mc-field-group">
                            <label for="mce-LNAME">Apellidos</label>
                            <input type="text" value="" name="LNAME" class="" id="mce-LNAME">
                        </div>
                        <div id="mce-responses" class="clear">
                            <div class="response" id="mce-error-response" style="display:none"></div>
                            <div class="response" id="mce-success-response" style="display:none"></div>
                        </div>
                        <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_4ba748fdbc9f3a9af7f361af8_79025bb69d" tabindex="-1" value=""></div>
                        <div class="clear"><input type="submit" value="Suscríbete" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
                    </div>
                </form>
            </div>
        </div>
        <script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script>
        <script type='text/javascript'>
            (function($) {
                window.fnames = new Array();
                window.ftypes = new Array();
                fnames[0] = 'EMAIL';
                ftypes[0] = 'email';
                fnames[1] = 'FNAME';
                ftypes[1] = 'text';
                fnames[2] = 'LNAME';
                ftypes[2] = 'text';
            }(jQuery));
            var $mcj = jQuery.noConflict(true);
        </script>
        <!--End mc_embed_signup-->
    </footer>

    <script src="js/vendor/modernizr-3.7.1.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script>
        window.jQuery || document.write('<script src="js/vendor/jquery-3.3.1.min.js"><\/script>')
    </script>
    <script src="js/plugins.js"></script>
    <script src="js/jquery.animateNumber.js"></script>
    <script src="js/jquery.countdown.js"></script>
    <script src="js/jquery.lettering.js"></script>
    <script src="https://unpkg.com/leaflet@1.5.1/dist/leaflet.js"></script>
    <?php
    $archivo = basename($_SERVER['PHP_SELF']);
    $pagina = str_replace(".php", "", $archivo);
    if ($pagina === 'invitados' || $pagina === 'index') {
        echo '<script src="js/jquery.colorbox-min.js"></script>';
    } else if ($pagina === 'conferencia') {
        echo '<script src="js/lightbox.js"></script>';
    }
    ?>
    <script src="js/main.js"></script>

    <!-- Google Analytics: change UA-XXXXX-Y to be your site's ID. -->
    <script>
        window.ga = function() {
            ga.q.push(arguments)
        };
        ga.q = [];
        ga.l = +new Date;
        ga('create', 'UA-XXXXX-Y', 'auto');
        ga('set', 'transport', 'beacon');
        ga('send', 'pageview')
    </script>
    <script src="https://www.google-analytics.com/analytics.js" async defer></script>
    </body>

    </html>