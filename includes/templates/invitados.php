<?php
    try {
        // Se crea conexión a la BBDD
        require_once('includes/functions/bbdd_conexion.php');
        // Consulta a la BBDD
        $sql = "SELECT * FROM `invitados`";
        // Variable que realiza la consulta a la BBDD
        $resultado = $connection->query($sql);
    } catch (\Exception $e) {
        echo $e->getMessage();
    }
?>
<!-- NUESTROS INVITADOS -->
<section class="seccion contenedor">
    <h2>Nuestros invitados</h2>
    <ul class="lista-invitados clearfix">
    <?php
        while($invitados = $resultado->fetch_assoc()) { ?>
            <li>
                <div class="invitado">
                    <a class="invitado-info" href="#invitado<?php echo $invitados['id_invitado']; ?>">
                        <img src="img/<?php echo $invitados['url_imagen']; ?>" alt="imagen invitado">
                        <p><?php echo $invitados['nombre_invitado'] . " " . $invitados['apellido_invitado'] ?></p>
                    </a>
                </div>
            </li>
            <!-- DETALLE INVITADO -->
            <div style="display:none;">
                <div class="invitado-info" id="invitado<?php echo $invitados['id_invitado']; ?>">
                    <h2><?php echo $invitados['nombre_invitado'] . " " .  $invitados['apellido_invitado']; ?></h2>
                    <img src="img/<?php echo $invitados['url_imagen']; ?>" alt="imagen invitado">
                    <p><?php echo $invitados['descripcion_invitado']; ?></p>
                </div>
            </div>
        <?php } ?>
    </ul>
</section>
<?php
    // Se cierra conexión a la BBDD
    $connection->close();
?>