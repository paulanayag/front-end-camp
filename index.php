<?php include_once 'includes/templates/header.php'; ?>
<!-- DESCRIPCIÓN DE LA PÁGINA WEB -->
<section class="seccion contenedor">
  <h2>La mejor conferencia de diseño web</h2>
  <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Non asperiores et corporis omnis, mollitia architecto, rerum hic reprehenderit illum dolore iste dolorem at quo esse ullam! Omnis reiciendis vel rerum.</p>
</section>
<!-- PROGRAMA DEL EVENTO -->
<section class="programa">
  <!-- VÍDEO -->
  <div class="contenedor-video">
    <video autoplay loop poster="bg-talleres.jpg">
      <source src="video/video.mp4" type="video/mp4">
      <source src="video/video.webm" type="video/webm">
      <source src="video/video.ogv" type="video/ogv">
    </video>
  </div>
  <!-- CONTENIDO PROGRAMA -->
  <div class="contenido-programa">
    <div class="contenedor">
      <div class="programa-evento">
        <h2>Programa del evento</h2>
        <?php
        try {
          // Se crea conexión a la BBDD
          require_once('includes/functions/bbdd_conexion.php');
          // Consulta a la BBDD
          $sql = " SELECT * FROM `categoria_evento` ";
          // Variable que realiza la consulta a la BBDD
          $resultado = $connection->query($sql);
        } catch (Exception $e) {
          $error = $e->getMessage();
        }
        ?>
        <!-- MENÚ PROGRAMA -->
        <nav class="menu-programa">
          <?php while ($categoria = $resultado->fetch_array(MYSQLI_ASSOC)) { ?>
            <?php $nombre_categoria = $categoria['categoria_evento']; ?>
            <a href="#<?php echo strtolower($nombre_categoria) ?>">
              <i class="fas <?php echo $categoria['icono_evento'] ?>"></i>
              <?php echo $categoria['categoria_evento'] ?>
            </a>
          <?php } ?>
        </nav>
        <?php
        try {
          // Se crea conexión a la BBDD
          require_once('includes/functions/bbdd_conexion.php');
          // Consulta a la BBDD
          $sql = " SELECT id_evento, nombre_evento, fecha_evento, hora_evento, categoria_evento, icono_evento, nombre_invitado, apellido_invitado ";
          $sql .= " FROM eventos ";
          $sql .= " INNER JOIN categoria_evento ";
          $sql .= " ON eventos.id_categoria_evento = categoria_evento.id_categoria ";
          $sql .= " INNER JOIN invitados ";
          $sql .= " ON eventos.id_invitado = invitados.id_invitado ";
          $sql .= " AND  eventos.id_categoria_evento = 1 ";
          $sql .= " ORDER BY id_evento LIMIT 2; ";

          $sql .= " SELECT id_evento, nombre_evento, fecha_evento, hora_evento, categoria_evento, icono_evento, nombre_invitado, apellido_invitado ";
          $sql .= " FROM eventos ";
          $sql .= " INNER JOIN categoria_evento ";
          $sql .= " ON eventos.id_categoria_evento = categoria_evento.id_categoria ";
          $sql .= " INNER JOIN invitados ";
          $sql .= " ON eventos.id_invitado = invitados.id_invitado ";
          $sql .= " AND  eventos.id_categoria_evento = 2 ";
          $sql .= " ORDER BY id_evento LIMIT 2; ";

          $sql .= " SELECT id_evento, nombre_evento, fecha_evento, hora_evento, categoria_evento, icono_evento, nombre_invitado, apellido_invitado ";
          $sql .= " FROM eventos ";
          $sql .= " INNER JOIN categoria_evento ";
          $sql .= " ON eventos.id_categoria_evento = categoria_evento.id_categoria ";
          $sql .= " INNER JOIN invitados ";
          $sql .= " ON eventos.id_invitado = invitados.id_invitado ";
          $sql .= " AND  eventos.id_categoria_evento = 3 ";
          $sql .= " ORDER BY id_evento LIMIT 2; ";
        } catch (Exception $e) {
          echo $e->getMessage();
        }
        ?>
        <?php $connection->multi_query($sql); ?>
        <?php
        do {
          $resultado = $connection->store_result();
          $row = $resultado->fetch_all(MYSQLI_ASSOC);
        ?>

        <?php $i = 0; ?>
        <?php foreach($row as $evento): ?>
          <?php if($i % 2 == 0) { ?>
          <!-- SEMINARIOS, CONFERENCIAS & TALLERES -->
          <div id="<?php echo strtolower($evento['categoria_evento']);?>" class="info-curso ocultar clearfix">
          <?php } ?>
            <div class="detalle-evento">
              <h3><?php echo mb_convert_encoding($evento["nombre_evento"],'UTF-8'); ?></h3>
              <p><i class="fas fa-clock"></i><?php echo $evento['hora_evento']; ?></p>
              <p><i class="fas fa-calendar"></i><?php echo $evento['fecha_evento']; ?></p>
              <p><i class="fas fa-user"></i><?php echo $evento['nombre_invitado'] . " " . $evento['apellido_invitado']; ?></p>
            </div>
          <?php if($i % 2 == 1) { ?>
            <a href="calendario.php" class="button float-right">Ver todos</a>
          </div>
          <?php } ?>
          <?php $i++; ?>
        <?php endforeach; ?>
        <?php $resultado->free(); ?>
        <?php
        } while ($connection->more_results() && $connection->next_result());
        ?>
      </div>
    </div>
  </div>
</section>
<!-- NUESTROS INVITADOS -->
<?php include_once 'includes/templates/invitados.php'; ?>
<!-- CONTADOR -->
<div class="contador parallax">
  <div class="contenedor">
    <ul class="resumen-evento clearfix">
      <li>
        <p class="numero"></p>Invitados
      </li>
      <li>
        <p class="numero"></p>Talleres
      </li>
      <li>
        <p class="numero"></p>Días
      </li>
      <li>
        <p class="numero"></p>Conferencias
      </li>
    </ul>
  </div>
</div>
<!-- PRECIOS -->
<section class="precios seccion">
  <h2>Precios</h2>
  <div class="contenedor">
    <ul class="lista-precios clearfix">
      <li>
        <div class="tabla-precio">
          <h3>Pase por día</h3>
          <p class="numero">$30</p>
          <ul>
            <li>Bocadillos Gratis</li>
            <li>Todas las conferencias</li>
            <li>Todos los talleres</li>
          </ul>
          <a href="#" class="button hollow">Comprar</a>
        </div>
      </li>
      <li>
        <div class="tabla-precio">
          <h3>Todos los días</h3>
          <p class="numero">$50</p>
          <ul>
            <li>Bocadillos Gratis</li>
            <li>Todas las conferencias</li>
            <li>Todos los talleres</li>
          </ul>
          <a href="#" class="button">Comprar</a>
        </div>
      </li>
      <li>
        <div class="tabla-precio">
          <h3>Pase por 2 días</h3>
          <p class="numero">$45</p>
          <ul>
            <li>Bocadillos Gratis</li>
            <li>Todas las conferencias</li>
            <li>Todos los talleres</li>
          </ul>
          <a href="#" class="button hollow">Comprar</a>
        </div>
      </li>
    </ul>
  </div>
</section>
<!-- MAPA -->
<div id="mapa" class="mapa">

</div>
<!-- TESTIMONIALES -->
<section class="seccion">
  <h2>Testimoniales</h2>
  <div class="testimoniales contenedor clearfix">
    <div class="testimonial">
      <blockquote>
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Reprehenderit laudantium voluptatem adipisci voluptatibus! Eius voluptate aperiam fugit saepe ex amet dolore eos repellat labore necessitatibus, nisi placeat vitae maxime asperiores.</p>
        <footer class="info-testimonial clearfix">
          <img src="img/testimonial.jpg" alt="imagen testimonial">
          <cite>Oswaldo Aponte Escobedo <span>Diseñador en @prisma</span> </cite>
        </footer>
      </blockquote>
    </div>
    <div class="testimonial">
      <blockquote>
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Reprehenderit laudantium voluptatem adipisci voluptatibus! Eius voluptate aperiam fugit saepe ex amet dolore eos repellat labore necessitatibus, nisi placeat vitae maxime asperiores.</p>
        <footer class="info-testimonial clearfix">
          <img src="img/testimonial.jpg" alt="imagen testimonial">
          <cite>Oswaldo Aponte Escobedo <span>Diseñador en @prisma</span> </cite>
        </footer>
      </blockquote>
    </div>
    <div class="testimonial">
      <blockquote>
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Reprehenderit laudantium voluptatem adipisci voluptatibus! Eius voluptate aperiam fugit saepe ex amet dolore eos repellat labore necessitatibus, nisi placeat vitae maxime asperiores.</p>
        <footer class="info-testimonial clearfix">
          <img src="img/testimonial.jpg" alt="imagen testimonial">
          <cite>Oswaldo Aponte Escobedo <span>Diseñador en @prisma</span> </cite>
        </footer>
      </blockquote>
    </div>
  </div>
</section>
<!-- NEWSLETTER -->
<div class="newsletter parallax">
  <div class="contenido contenedor">
    <p>regístrate al newletter:</p>
    <h3>FrontEndCamp</h3>
    <a href="#mc_embed_signup" class="button_newsletter button transparente">Registro</a>
  </div>
</div>
<!-- CUENTA REGRESIVA -->
<section class="seccion">
  <h2>Faltan</h2>
  <div class="cuenta-regresiva">
    <ul class="clearfix contenedor">
      <li>
        <p id="dias" class="numero"></p>días
      </li>
      <li>
        <p id="horas" class="numero"></p>horas
      </li>
      <li>
        <p id="minutos" class="numero"></p>minutos
      </li>
      <li>
        <p id="segundos" class="numero"></p>segundos
      </li>
    </ul>
  </div>
</section>
<?php include_once 'includes/templates/footer.php'; ?>