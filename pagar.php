<?php

require 'includes/paypal.php';

use PayPal\Api\Amount;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;

if (!isset($_POST['submit'])) {
  exit("Hubo un error");
}

if (isset($_POST['submit'])) {
  $nombre = isset($_POST['nombre']) ? $_POST['nombre'] : '';
  $apellido = isset($_POST['apellido']) ? $_POST['apellido'] : '';
  $email = isset($_POST['email']) ? $_POST['email'] : '';
  $fecha = date('Y-m-d H:i:s');
  $boletos = isset($_POST['boletos']) ? $_POST['boletos'] : [];
  $auxBoletos = $boletos;
  $pedidoExtra = isset($_POST['pedido_extra']) ? $_POST['pedido_extra'] : [];
  $pedidoCamisetas = isset($_POST['pedido_extra']['camisetas']['cantidad']) ? $_POST['pedido_extra']['camisetas']['cantidad'] : '';
  $precioCamisetas = isset($_POST['pedido_extra']['camisetas']['precio']) ? $_POST['pedido_extra']['camisetas']['precio'] : '';
  $pedidoEtiquetas = isset($_POST['pedido_extra']['etiquetas']['cantidad']) ? $_POST['pedido_extra']['etiquetas']['cantidad'] : '';
  $precioEtiquetas = isset($_POST['pedido_extra']['etiquetas']['precio']) ? $_POST['pedido_extra']['etiquetas']['precio'] : '';
  $eventos = isset($_POST['registro']) ? $_POST['registro'] : [];
  $regalo = isset($_POST['regalo']) ? $_POST['regalo'] : '';
  $total = isset($_POST['total_pedido']) ? $_POST['total_pedido'] : '';
  
  include_once 'includes/functions/funciones.php';
  $pedido = productos_json($boletos, $pedidoCamisetas, $pedidoEtiquetas);
  $registro = evento_json($eventos);

  try {
    // Se crea conexión a la BBDD
    require_once('includes/functions/bbdd_conexion.php');
    // Prepared statements para insertar datos en la BBDD
    $stmt = $connection->prepare("INSERT INTO registrados (nombre_registrado, apellido_registrado, email_registrado, fecha_registro, pases_articulos, talleres_registrados, regalo, total_pagado) VALUES (?, ?, ?, ?, ?, ?, ?, ?)");
    $stmt->bind_param("ssssssis", $nombre, $apellido, $email, $fecha, $pedido, $registro, $regalo, $total);
    $stmt->execute();
    $ID_registro = $stmt->insert_id;
    $stmt->close();
    $connection->close();
    //header('Location: validar_registro.php?success=1');
  } catch (Exception $e) {
    echo $e->getMessage();
  }
}

$compra = new Payer();
$compra->setPaymentMethod('paypal');

$i = 0;
$arrayPedido = array();
foreach($auxBoletos as $key => $value) {
  if((int) $value['cantidad'] > 0) {
    ${"articulo$i"} = new Item();
    $arrayPedido[] = ${"articulo$i"};
    ${"articulo$i"}->setName('Pase: ' . $key)
      ->setCurrency('USD')
      ->setQuantity((int) $value['cantidad'])
      ->setPrice((float) $value['precio']);
    $i++;
  }
}
foreach($pedidoExtra as $key => $value) {
  if((int) $value['cantidad'] > 0) {
    if($key === 'camisetas') {
      $precio = (float) $value['precio'] * 0.93;
    } else {
      $precio = (float) $value['precio'];
    }
    ${"articulo$i"} = new Item();
    $arrayPedido[] = ${"articulo$i"};
    ${"articulo$i"}->setName('Extras: ' . $key)
      ->setCurrency('USD')
      ->setQuantity((int) $value['cantidad'])
      ->setPrice((float) $precio);
    $i++;
  }
}

$listaArticulos = new ItemList();
$listaArticulos->setItems($arrayPedido);

$cantidad = new Amount();
$cantidad->setCurrency('USD')
  ->setTotal($total);

$transaccion = new Transaction();
$transaccion->setAmount($cantidad)
  ->setItemList($listaArticulos)
  ->setDescription('Pago frontEndCamp')
  ->setInvoiceNumber($ID_registro);

$redireccionar = new RedirectUrls();
$redireccionar->setReturnUrl(URL_SITIO . "/pago_finalizado.php?exito=true&id_pago={$ID_registro}")
  ->setCancelUrl(URL_SITIO . "/pago_finalizado.php?exito=false&id_pago={$ID_registro}");

$pago = new Payment();
$pago->setIntent("sale")
  ->setPayer($compra)
  ->setRedirectUrls($redireccionar)
  ->setTransactions(array($transaccion));

try {
  $pago->create($apiContext);
} catch (PayPal\Exception\PayPalConnectionException $pce) {
  echo '<pre>';
  print_r(json_decode($pce->getData()));
  exit;
}

$aprobado = $pago->getApprovalLink();

header("Location: {$aprobado}");
