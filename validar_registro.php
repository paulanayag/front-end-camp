<?php if(isset($_POST['submit'])):
    $nombre = isset($_POST['nombre']) ? $_POST['nombre'] : '';
    $apellido = isset($_POST['apellido']) ? $_POST['apellido'] : '';
    $email = isset($_POST['email']) ? $_POST['email'] : '';
    $fecha = date('Y-m-d H:i:s');
    $boletos = isset($_POST['boletos']) ? $_POST['boletos'] : [];
    $pedidoExtra = isset($_POST['pedido_extra']) ? $_POST['pedido_extra'] : [];
    $eventos = isset($_POST['registro']) ? $_POST['registro'] : [];
    $regalo = isset($_POST['regalo']) ? $_POST['regalo'] : '';
    $total = isset($_POST['total_pedido']) ? $_POST['total_pedido'] : '';

    try {
        // Se crea conexión a la BBDD
        require_once('includes/functions/bbdd_conexion.php');
        // Prepared statements para insertar datos en la BBDD
        $stmt = $connection->prepare("INSERT INTO registrados (nombre_registrado, apellido_registrado, email_registrado, fecha_registro, pases_articulos, talleres_registrados, regalo, total_pagado) VALUES (?, ?, ?, ?, ?, ?, ?, ?)");
        $stmt->bind_param("ssssssis", $nombre, $apellido, $email, $fecha, $pedido, $registro, $regalo, $total);
        $stmt->execute();
        $stmt->close();
        $connection->close();
        header('Location: validar_registro.php?success=1');
    } catch (Exception $e) {
        echo $e->getMessage();
    }
?>
<?php endif; ?>
<?php include_once 'includes/templates/header.php'; ?>
<section class="seccion contenedor">
    <h2>Resumen registro</h2>
    <?php
        if(isset($_GET['success'])):
            if($_GET['success'] == 1):
                echo 'Ha sido registrado con éxito';
            endif;
        endif;
    ?>
</section>
<?php include_once 'includes/templates/footer.php'; ?>