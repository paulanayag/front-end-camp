<?php include_once 'includes/templates/header.php'; ?>
    <!-- CALENDARIO DE EVENTOS -->
    <section class="seccion contenedor">
        <h2>Calendario de Eventos</h2>
        <?php
            try {
                // Se crea conexión a la BBDD
                require_once('includes/functions/bbdd_conexion.php');
                // Consulta a la BBDD
                $sql = " SELECT id_evento, nombre_evento, fecha_evento, hora_evento, categoria_evento, icono_evento, nombre_invitado, apellido_invitado ";
                $sql .= " FROM eventos ";
                $sql .= " INNER JOIN categoria_evento ";
                $sql .= " ON eventos.id_categoria_evento = categoria_evento.id_categoria ";
                $sql .= " INNER JOIN invitados ";
                $sql .= " ON eventos.id_invitado = invitados.id_invitado ";
                $sql .= " ORDER BY id_evento ";
                // Variable que realiza la consulta a la BBDD
                $resultado = $connection->query($sql);
            } catch (Exception $e) {
                echo $e->getMessage();
            }
        ?>
        <!-- CALENDARIO -->
        <div class="calendario">
            <?php
                $calendario = array();
                // Se imprime resultados de la consulta
                while ($eventos = $resultado->fetch_assoc()) { 
                    $fecha = $eventos['fecha_evento'];
                    $evento = array (
                        'titulo' => $eventos['nombre_evento'],
                        'fecha' => $eventos['fecha_evento'],
                        'hora' => $eventos['hora_evento'],
                        'categoria' => $eventos['categoria_evento'],
                        'icono' => "fa " . $eventos['icono_evento'],
                        'invitado' => $eventos['nombre_invitado'] . " " . $eventos['apellido_invitado']
                    );
                    $calendario[$fecha][] = $evento;
                }
                ?>
                <?php
                // Eventos agrupados por fechas
                foreach($calendario as $dia => $lista_eventos) {
                    ?>
                    <h3>
                        <i class="fa fa-calendar"></i>
                        <?php
                            setlocale(LC_TIME, 'spanish');
                            echo strftime("%d de %B del %Y", strtotime($dia)); 
                        ?>
                    </h3>
                    <?php
                    foreach($lista_eventos as $evento) {
                    ?>
                        <div class="evento">
                            <p class="titulo"><?php echo $evento['titulo'] ?></p>
                            <p class="hora">
                                <i class="far fa-clock" aria-hidden="true"></i>
                                <?php echo $evento['hora'] ?>
                            </p>
                            <p>
                            <i class="<?php echo $evento['icono']; ?>" aria-hidden="true"></i>
                                <?php echo $evento['categoria'] ?>
                            </p>
                            <p>
                                <i class="far fa-user" aria-hidden="true"></i>
                                <?php echo $evento['invitado'] ?>
                            </p>
                        </div>
                    <?php
                    }
                    ?>
                <?php    
                }
                ?>
        </div>
        <?php
                // Se cierra conexión a la BBDD
                $connection->close();
            ?>
    </section>
<?php include_once 'includes/templates/footer.php'; ?>